const express = require("express");
const makeStoppable = require("stoppable");
const http = require("http");
const indexRoutes = require("./routes/index");
const doneRoutes = require("./routes/done");

const app = express();

app.set("view engine", "ejs");

app.use(express.urlencoded({ extended: false }));
app.use(express.static("public"));

app.use(indexRoutes);
app.use(doneRoutes);

const server = makeStoppable(http.createServer(app));

module.exports = () => {
  const stopServer = () => {
    return new Promise((resolve) => {
      server.stop(resolve);
    });
  };

  return new Promise((resolve) => {
    server.listen(3000, () => {
      console.log("Express server is listening on http://localhost:3000");
      resolve(stopServer);
    });
  });
};
