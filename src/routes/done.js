const express = require("express");
const { Task, tasks } = require("../tasks");

const router = express.Router();

router.get("/done", async (req, res, next) => {
  res.render("pages/done", {
    taskList: await tasks.fetchDone(),
  });
});

router.post("/api/tasks/:task/done", async (req, res, next) => {
  let { task } = req.params;

  await tasks.done(task);

  res.render("pages/index", {
    taskList: await tasks.fetchNotDone(),
    success: null,
  });
});

module.exports = router;
