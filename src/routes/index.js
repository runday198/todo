const express = require("express");
const { Task, tasks } = require("../tasks");

const router = express.Router();

router.get("/", async (req, res, next) => {
  res.render("pages/index", {
    taskList: await tasks.fetchNotDone(),
    success: null,
  });
});

router.post("/", async (req, res, next) => {
  let { task } = req.body;

  if (task.length >= 3 && (await tasks.findTask(task.trim())) === false) {
    task = new Task(task.trim());
    tasks.addTask(task);

    res.render("pages/index", {
      taskList: await tasks.fetchNotDone(),
      success: true,
    });
  } else {
    res.render("pages/index", {
      taskList: await tasks.fetchNotDone(),
      success: false,
      task: task,
    });
  }
});

module.exports = router;
