class Task {
  constructor(name) {
    this.name = name;
    this.done = false;
  }
}

class TaskList {
  constructor() {
    this.tasks = [];
  }

  findTask(task) {
    return new Promise((resolve, reject) => {
      let taskIndex = this.tasks.findIndex((taskObj) => taskObj.name === task);
      if (taskIndex === -1) {
        resolve(false);
      } else {
        resolve(this.tasks[taskIndex]);
      }
    });
  }

  addTask(task) {
    return new Promise((resolve, reject) => {
      this.tasks.unshift(task);
      resolve(this.tasks);
    });
  }

  done(name) {
    return new Promise((resolve, reject) => {
      for (let task of this.tasks) {
        if (name === task.name) {
          task.done = true;
        }
      }
      resolve();
    });
  }

  fetchDone() {
    return new Promise((resolve, reject) => {
      let doneTasks = this.tasks.filter((task) => task.done === true);
      resolve(doneTasks);
    });
  }

  fetchNotDone() {
    return new Promise((resolve, reject) => {
      let unDoneTasks = this.tasks.filter((task) => task.done === false);
      resolve(unDoneTasks);
    });
  }
}

const tasks = new TaskList();

module.exports = {
  tasks,
  Task,
};
